# -*- coding: utf-8 -*-
"""
Created on Tue Aug 09 18:33:22 2016

@author: Administrator
"""

""" This set of modules are helpers for production data digitization

"""
from cStringIO import StringIO
from pdfminer.converter import LTChar

from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter

from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
import csv  
from time import sleep
import numpy as np

def new_line_csv(rowlist , csvname):
    """ Add a new row to the existing csvname file 
    """
    with open(csvname+".csv", "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        wr.writerow(rowlist)


import re
rxcountpages = re.compile(r"/Type\s*/Page([^s]|$)", re.MULTILINE|re.DOTALL)
def count_pages(filename):
    data = file(filename,"rb").read()
    return len(rxcountpages.findall(data))


        

def pdf_to_txt(page, pagenum, totalnumpage, separator, threshold, ybot, ytop, xleft, xright):
    """ This function converts a pdf file to CSV

    """


    class CsvConverter(TextConverter):
        """ class that converts a pdf into a csv string

        """
        def __init__(self, *args, **kwargs):
            TextConverter.__init__(self, *args, **kwargs)
            self.separator = separator
            self.threshold = threshold
            self.ytop = ytop
            self.ybot = ybot
            self.xleft = xleft
            self.xright = xright
            self.locs = []

        def end_page(self, i):
            from collections import defaultdict
            lines = defaultdict(lambda: {})
            global locations
            locations = defaultdict(lambda: {})
            for child in self.cur_item._objs:  # <-- changed
                if isinstance(child, LTChar):
                    (_, _, x, y) = child.bbox
                    if y <= self.ytop and y >= self.ybot: # if it is between the area specified from top and bottom
                        line = lines[int(-y)]
                        
                        
                        if x>=xleft and x<=xright: # if between specified left and right limits
                            line[x] = child._text.encode(self.codec)  # <-- changed
                            #print y
                            #print line
                            
                        """
                        if x>=xleft and x<=xright: # if between specified left and right limits
                            line[x] = child._text.encode(self.codec)  # <-- changed
                        #if (min(line.keys())-xleft)>=self.threshold: # if first column is empty
                        #    line[xleft] = ''
                        if (xright-max(line.keys()))>=self.threshold:
                            line[xright] = ''
                        """
            for y in sorted(lines.keys()):
                
                    
                line = lines[y]
                
                # account for missing first and last columns data
                #if (min(line.keys())-xleft)>=self.threshold: # if first column is empty
                #    line[xleft] = ''
                
                if (xright-max(line.keys()))>=1*self.threshold:
                    line[xright] = ''
        
                
                linetxt , locations = self.line_creator(line)
                
                # get the aggregated text
                self.outfp.write(linetxt)
                self.outfp.write("\n")
                # get the starting locations for each text component 
                # (separated by commas)
                #self.locatios.write(locations)
                self.locs.append(locations)  
        def line_creator(self, line):
            """ find connected texts by seeing if their ditance difference is
            less than  a threshold

            """
            
            keys = sorted(line.keys())
            # calculate the average distange between each character on this row
            #average_distance1 = sum([keys[i] - keys[i - 1] for i in range(1, len(keys))]) / len(keys)
            average_distance = 1
            # append the first character to the result
            result = [line[keys[0]]]
            locations = [keys[0]]
            for i in range(1, len(keys)):
                
                
      
                    
                # if the distance between this character and the last 
                # character is greater than the average*threshold
                diffkeys = keys[i]-keys[i-1]
                if diffkeys > (average_distance * self.threshold):
                    # append the separator into that position
                    result.append(self.separator)
                    #if diffkeys > 10*self.threshold:
                    #    result.append(self.separator)
                     
                    numsep = 1
                    for j in xrange(int(diffkeys/(8*self.threshold))):
                        result.append(self.separator)
                        numsep += 1
                        #if numsep > 2: # no more than two consecutive seperators
                        #    break
                    
                    locations.append(keys[i])
                    
                # append the character
                result.append(line[keys[i]])
                
            printable_line = ''.join(result)
            return printable_line , locations

    # ... the following part of the code is a remix of the
    # convert() function in the pdfminer/tools/pdf2text module
    rsrc = PDFResourceManager()
    outfp = StringIO()
    
    
    laparams = LAParams()
    laparams.detect_vertical = False
    laparams.char_margin = 2.0 # 2.0
    laparams.line_margin = 2.0 # 0.5
    laparams.word_margin = 0.2 # 0.1
    laparams.boxes_flow = -0.5 # Specifies how much a horizontal and vertical
    # position of a text matters when determining a text order. The value 
    #should be within the range of -1.0 (only horizontal position matters) 
    # to +1.0 (only vertical position matters). The default value is 0.5.
    
    # Create a PDF device object.
    outtype = 'csv'
    if outtype == 'text':
        device = TextConverter(rsrc, outfp, codec="utf-8", laparams=laparams)
    elif outtype == 'html':
        device = HTMLConverter(rsrc, outfp, codec="utf-8", laparams=laparams)
    elif outtype == 'xml':
        device = XMLConverter(rsrc, outfp, codec="utf-8", laparams=laparams)
    elif outtype == 'csv':
        device = CsvConverter(rsrc, outfp, codec="utf-8", laparams=laparams)
    # becuase my test documents are utf-8 (note: utf-8 is the default codec)

    #pdfobj = open(filename, 'rb')
    interpreter = PDFPageInterpreter(rsrc, device)
    #LOL_pagesdata = []
    #totalnumpage = count_pages(filename)
    #for i, page in enumerate(PDFPage.get_pages(pdfobj)):
        #print "Digitizing Page Number {} out of total {}".format(i,totalnumpage-1)
        #page = PDFPage.get_pages(fp)[i]        
    outfp.write("START PAGE %d\n" % pagenum)
    if page is not None:
        #print 'none'
        interpreter.process_page(page)
    outfp.write("END PAGE %d\n" % pagenum)
    device.close()
    #pdfobj.close()
    pagetxt = outfp.getvalue()
    return pagetxt


def find_str(s, char):
    """ This function finds location of a given string in a longer string

    args:
        s: original long string
        char: the substring to search for
    outputs:
        index: location of  start of the char string inside s
        and location of end of the char inside s
        will return -1,-1 if char is not found

    """
    index = 0
    if char in s:
        c = char[0]
        for ch in s:
            if ch == c:
                if s[index:index+len(char)] == char:
                    return index, index+len(char)
            index += 1
    return -1, -1




    




def split_txt_pages(all_pages_txt,seperator):
    """ This function extracts page by page texts from all_pages_text
    using the start and end page strings used in pdf_to_txt file.
    
    """
    separatedpages_txt = []
    for i in xrange(0,9999):
        start_char = 'START PAGE {}'.format(i)
        end_char = 'END PAGE {}'.format(i)
        endstart,endend = find_str(all_pages_txt,end_char)
        startstart,startend = find_str(all_pages_txt,start_char)
        if endstart == -1:
            break
        else:
            # extract data part from table (only gor GOM data)
            separatedpages_txt.append(all_pages_txt[startstart:endend])
    return separatedpages_txt   





from calendar import monthrange
from datetime import datetime
def assigntypes_to_LOL(pagetxt_LoL, col_names):
    """ gets all string list and corrects the structure based on type
        from GOM data format, we know the first element is always date in 
        specific forms yyyymm -> terra.ai format 
    
    """

    
    
    data_LoL = []
    for linenum,linedata in enumerate(pagetxt_LoL):
        
        new_row = []
        for colnum,elem in enumerate(linedata):

            if elem == '': # missing data in the pdf document
                newelem = np.nan
            else: # data is scraped and is available
                if colnum == 0: # report date month
                        inputdate = datetime.strptime(elem,'%Y%m').date()
                        year = inputdate.year
                        month = inputdate.month
                        day = monthrange(inputdate.year,inputdate.month)[1]
                        inputdate = datetime(year,month,day).date()
                        newelem = inputdate
                
                elif colnum == 8: # completion date column
                        if elem == []: # missing completion date
                            inputdate = np.nan 
                        else:
                            inputdate = datetime.strptime(elem,'%Y%m%d').date()
                        year = inputdate.year
                        month = inputdate.month
                        day = monthrange(inputdate.year,inputdate.month)[1]
                        inputdate = datetime(year,month,day).date()
                        newelem = inputdate
                elif [2,3,4,5,7].__contains__(colnum) : 
                        # columns that we know should be strings
                        newelem = elem.strip()
                elif elem.strip().isdigit() == True: # integer values like API, oil prod, etc
                        newelem = int(elem.strip())
                else: # combined character values and integers
                        newelem = elem.strip()
            new_row.append(newelem)
        data_LoL.append(new_row)         
    return data_LoL

    
        
def post_process_each_page(pagetxt, separator, 
                           pagenum, pdfname):
    """ this function finds the column data and organizes the columns data for 
    each page
    
    """
    
    # convert page string to array by splitting lines
    pagelines_splitted = pagetxt.splitlines()
    
    # now select only table data part and remove header and footers
    startline = 0
    endline = len(pagelines_splitted)
    for lin,txt in enumerate(pagelines_splitted):
        startindx,_ = find_str(txt,'START PAGE')
        if startindx!=-1:
            startline = lin+1
            continue
        endindx,_ = find_str(txt,'UNCLASSIFIED')
        if endindx !=-1:
            endline = lin
            break
        else:
            endindx,_ = find_str(txt,'END PAGE')
        if endindx !=-1:
            endline = lin
            break  
    pagelines_splitted = pagelines_splitted[startline:endline]
    
    # split each line based on separator "," and put in lists
    pagetxt_LoL = [txtlin.split(separator) for txtlin in pagelines_splitted]
    
    # attach pdf name, page number and row number to each list, for QC checks
    # later on
    pagetxt_LoL = [row+[pdfname,str(pagenum),str(i)] for i,row in enumerate(pagetxt_LoL)]
    
    # column names used to push to database
    header_names = ['report_date_mo', 'operator_no', 'field_no', 'lease_no',
                    'unit', 'well_name', 'API_prod', 'completion_int', 
                    'completion_date', 'prod_days', 'oil_mo', 'gas_mo', 
                    'water_mo', 'injection_vol', 'well_status', 
                    'pdf_name', 'page_no', 'row_no']
    
    # assign proper data types to each element
    padedata_LoL = assigntypes_to_LOL(pagetxt_LoL, header_names)
    return padedata_LoL, header_names   




def write_rows_to_DB(conn, cursor, data_LoL, headers, schemaname, tablename, drop_table=False):
    """
    Write the results to the PostgreSQL database. The column names will be same
    as the headers. this function assumes data_LoL is a list of lists, in which
    each list contains information for each row and each inner list contains
    the data for columns in that row
    lists need to be of the same length, otherwise this method will give an
    Index out of range error. This function accepts both formats -


    Type constraints -


    Args:
        conn : Database connection object

        cursor : Database connection object

        results_data : Data to be written to the database.
                       Can be a List of dictionarys or Dictionary of Lists.

        verbosity : Boolean flag. If true function will print summary of func execution to console.

        schema : String object. Name of schema in which to create the table.

        table_name : String object. Name of the database table to write data

        drop_table : Boolean object. If true will drop table of same table name if it exists

    Returns:
        Nothing

    """
    # Drop table if exists
    if drop_table:
        try:
            query = """DROP TABLE IF EXISTS {}."{}";""".format(schemaname, tablename)
            cursor.execute(query)
            conn.commit()
        except Exception as error:
            print error
            print "Error while dropping table"

    # defined in post+proces function above
    """
    header_names = ['report_date_mo', 'operator_no', 'field_no', 'lease_no',
                        'unit', 'well_name', 'API_prod', 'completion_int',
                        'completion_date', 'prod_days', 'oil_mo', 'gas_mo',
                        'water_mo', 'injection_vol', 'well_status',
                        'pdf_name', 'page_no', 'row_no']
    """

    types = ["DATE", "BIGINT", "VARCHAR(255)", "VARCHAR(255)",
             "VARCHAR(255)", "VARCHAR(255)", "BIGINT", "VARCHAR(255)",
             "DATE", "INT", "FLOAT", "FLOAT",
             "FLOAT", "FLOAT", "VARCHAR(255)",
             "VARCHAR(955)", "INT", "INT" ]

    # creat table query
    structure = [headers[i]+" "+types[i] for i in xrange(len(headers))]
    structure = ",".join(structure)
    query = """CREATE TABLE IF NOT EXISTS {}."{}" ({})""".format(schemaname, tablename, structure)
    cursor.execute(query)
    conn.commit()

    colnames = "(" + ','.join(headers)+")"
    #Batch insert using mogrify approach
    numrows = len(data_LoL)
    numcols = len(headers)
    template = "("+",".join(["%s"]*numcols)+")"
    step_size = 10000
    n_batch = int(np.ceil(numrows/step_size))
    if n_batch == 0:
        n_batch = 1
    ind = [min(x*step_size, numrows) for x in range(n_batch+1)]

    for temp_indx in xrange(n_batch):
        args_str = ','.join(cursor.mogrify(template, val)for val in data_LoL[ind[temp_indx]:ind[temp_indx+1]])

        query = """INSERT INTO {}."{}" {} VALUES {};""".format(
        schemaname, tablename, colnames, args_str)
        cursor.execute(query)

    conn.commit()



    return










def GOM_digitization_core(pdfname , separator=',' , threshold=10, 
                          ytop=0, ybot=99999, xleft=0, xright=99999):
    """ This function is the core for digitizing and cleaning GOM data
    
    """
    explicitpdfname = pdfname.split('/')[-1].split('.')[-2]
    
    pdfobj       = open(pdfname, 'rb')
    totalnumpage = count_pages(pdfname)
    
    alldata_LoL = []
    for pagenum, page in enumerate(PDFPage.get_pages(pdfobj)):
        if pagenum>5:
            break
        print "pdf name is: " + explicitpdfname
        print "Digitizing Page Number {} out of total {}".format(pagenum+1,totalnumpage)
                
    
    
        # single string contaiing all text extracted from pdf pages
        single_page_txt = pdf_to_txt(page, pagenum, totalnumpage, separator, 
                                     threshold, ybot, ytop, xleft, xright)
    

        # extract table prod data and concatenate the extracted pages into a 
        # single list of lists, each list memebr is a row and each internal 
        # list is column
    
    
        print "Post-Processing Page Number: {}".format(pagenum) 
        # concatenate the extracted lists after extraction of text
        data_singlepage_LoL , header_names = \
        post_process_each_page(single_page_txt, 
                               separator,
                               pagenum,
                               explicitpdfname) 

        # attach processed page data to dictionary 
        alldata_LoL = alldata_LoL + data_singlepage_LoL
        
        # write page by page to database
        #print "writing data to DB for pagenum: {}".format(pagenum)
        #write_rows_to_DB(conn, cursor, data_singlepage_LoL, header_names, 
        #                 wrSchema, wrTable, drop_table=False)  
        
        
    return alldata_LoL, header_names
    
    
    
    
    
def split_pdf(pdfname , pagenums):
    from pyPdf import PdfFileWriter, PdfFileReader
    inputpdf = PdfFileReader(open(pdfname, "rb"))
    pdflistname = []
    for i in pagenums: #xrange(inputpdf.numPages):
        output = PdfFileWriter()
        output.addPage(inputpdf.getPage(i))
        pdfsavedname = pdfname.split('.')[0]+"_page%s.pdf" % i
        with open(pdfsavedname, "wb") as outputStream:
            output.write(outputStream)        
        pdflistname.append(pdfsavedname)
    return pdflistname
###############################################################################
if __name__ == '__main__':
    # the separator to use with the CSV
    SEPARATOR = ','
    # the distance multiplier after which a character is considered part of a
    # new word/column/block. Usually 1.5 works quite well
    THRESHOLD = 10
    TEXT = pdf_to_txt('data/page-03.pdf', SEPARATOR, THRESHOLD)
    print TEXT